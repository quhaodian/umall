package com.haoxuer.umall.api.domain.request;

import lombok.Data;

@Data
public class ProductSearchRequest extends PlatformPageRequest {

    private String name;
}
