package com.haoxuer.umall.api.domain.simple;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductSimple implements Serializable {


    private Long id;

    private String name;

    private String logo;

    private Integer num;

    private Integer catalog;

}
