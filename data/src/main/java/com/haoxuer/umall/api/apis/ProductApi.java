package com.haoxuer.umall.api.apis;

import com.haoxuer.umall.api.domain.list.CategoryList;
import com.haoxuer.umall.api.domain.list.ProductList;
import com.haoxuer.umall.api.domain.page.ProductPage;
import com.haoxuer.umall.api.domain.request.PlatformRequest;
import com.haoxuer.umall.api.domain.request.ProductSearchRequest;

public interface ProductApi {

    /**
     *
     * 获取某个平台的所有产品
     *
     * @return
     */
    ProductList all(PlatformRequest request);


    /**
     * 根据商品名称进行搜索
     * @param request
     * @return
     */
    ProductPage search(ProductSearchRequest request);


    /**
     * 获取所有分类
     * @param request
     * @return
     */
    CategoryList allCategory(PlatformRequest request);
}
