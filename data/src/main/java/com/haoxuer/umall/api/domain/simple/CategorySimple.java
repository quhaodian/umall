package com.haoxuer.umall.api.domain.simple;

import lombok.Data;

import java.io.Serializable;

@Data
public class CategorySimple implements Serializable {

    private String name;

    private Integer id;

    private Integer pid;


}
