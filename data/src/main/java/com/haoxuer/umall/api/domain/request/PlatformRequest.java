package com.haoxuer.umall.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;
import lombok.Data;

@Data
public class PlatformRequest extends RequestUserTokenObject {

    private Long platform;
}
