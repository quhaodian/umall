/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 退货项
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_returns_item")
public class ReturnsItem extends AbstractEntity {

	private static final long serialVersionUID = -4112374596087084162L;

	/** 商品编号 */
	private String sn;

	/** 商品名称 */
	private String name;

	/** 数量 */
	private Integer quantity;

	/** 退货单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Returns returns;



}