/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 会员等级
 * 
 * 
 * 
 */
@Entity
@Table(name = "bs_member_rank")
public class MemberRank extends AbstractEntity {

	private static final long serialVersionUID = 3599029355500655209L;

	/** 名称 */
	private String name;

	/** 优惠比例 */
	private Double scale;

	/** 消费金额 */
	private BigDecimal amount;

	/** 是否默认 */
	private Boolean isDefault;

	/** 是否特殊 */
	private Boolean isSpecial;

	/** 会员 */
	@OneToMany(mappedBy = "memberRank", fetch = FetchType.LAZY)
	private Set<Member> members = new HashSet<Member>();

	/** 促销 */
	@ManyToMany(mappedBy = "memberRanks", fetch = FetchType.LAZY)
	private Set<Promotion> promotions = new HashSet<Promotion>();



}