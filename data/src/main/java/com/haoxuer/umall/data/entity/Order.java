/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Entity - 订单
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_order")
public class Order extends AbstractEntity {


	/**
	 * 订单状态
	 */
	public enum OrderStatus {

		/** 未确认 */
		unconfirmed,

		/** 已确认 */
		confirmed,

		/** 已完成 */
		completed,

		/** 已取消 */
		cancelled
	}

	/**
	 * 支付状态
	 */
	public enum PaymentStatus {

		/** 未支付 */
		unpaid,

		/** 部分支付 */
		partialPayment,

		/** 已支付 */
		paid,

		/** 部分退款 */
		partialRefunds,

		/** 已退款 */
		refunded
	}

	/**
	 * 配送状态
	 */
	public enum ShippingStatus {

		/** 未发货 */
		unshipped,

		/** 部分发货 */
		partialShipment,

		/** 已发货 */
		shipped,

		/** 部分退货 */
		partialReturns,

		/** 已退货 */
		returned
	}

	/** 订单编号 */
	private String sn;

	/** 订单状态 */
	private OrderStatus orderStatus;

	/** 支付状态 */
	private PaymentStatus paymentStatus;

	/** 配送状态 */
	private ShippingStatus shippingStatus;

	/** 支付手续费 */
	private BigDecimal fee;

	/** 运费 */
	private BigDecimal freight;

	/** 促销折扣 */
	private BigDecimal promotionDiscount;

	/** 优惠券折扣 */
	private BigDecimal couponDiscount;

	/** 调整金额 */
	private BigDecimal offsetAmount;

	/** 已付金额 */
	private BigDecimal amountPaid;

	/** 赠送积分 */
	private Long point;

	/** 收货人 */
	private String consignee;

	/** 地区名称 */
	private String areaName;

	/** 地址 */
	private String address;

	/** 邮编 */
	private String zipCode;

	/** 电话 */
	private String phone;

	/** 是否开据发票 */
	private Boolean isInvoice;

	/** 发票抬头 */
	private String invoiceTitle;

	/** 税金 */
	private BigDecimal tax;

	/** 附言 */
	private String memo;

	/** 促销 */
	private String promotion;

	/** 到期时间 */
	private Date expire;

	/** 锁定到期时间 */
	private Date lockExpire;

	/** 是否已分配库存 */
	private Boolean isAllocatedStock;

	/** 支付方式名称 */
	private String paymentMethodName;

	/** 配送方式名称 */
	private String shippingMethodName;

	/** 地区 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Area area;

	/** 支付方式 */
	@ManyToOne(fetch = FetchType.LAZY)
	private PaymentMethod paymentMethod;

	/** 配送方式 */
	@ManyToOne(fetch = FetchType.LAZY)
	private ShippingMethod shippingMethod;

	/** 操作员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member operator;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 优惠码 */
	@ManyToOne(fetch = FetchType.LAZY)
	private CouponCode couponCode;

	/** 优惠券 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_order_coupon")
	private List<Coupon> coupons = new ArrayList<Coupon>();

	/** 订单项 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("isGift asc")
	private List<OrderItem> orderItems = new ArrayList<OrderItem>();

	/** 订单日志 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<OrderLog> orderLogs = new HashSet<OrderLog>();

	/** 预存款 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
	private Set<Deposit> deposits = new HashSet<Deposit>();

	/** 收款单 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<Payment> payments = new HashSet<Payment>();

	/** 退款单 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<Refunds> refunds = new HashSet<Refunds>();

	/** 发货单 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<Shipping> shippings = new HashSet<Shipping>();

	/** 退货单 */
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<Returns> returns = new HashSet<Returns>();



}