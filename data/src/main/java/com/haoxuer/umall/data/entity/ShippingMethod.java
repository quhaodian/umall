/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 配送方式
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_shipping_method")
public class ShippingMethod extends OrderEntity {

	private static final long serialVersionUID = 5873163245980853245L;

	/** 名称 */
	private String name;

	/** 首重量 */
	private Integer firstWeight;

	/** 续重量 */
	private Integer continueWeight;

	/** 首重价格 */
	private BigDecimal firstPrice;

	/** 续重价格 */
	private BigDecimal continuePrice;

	/** 图标 */
	private String icon;

	/** 介绍 */
	private String description;

	/** 默认物流公司 */
	@ManyToOne(fetch = FetchType.LAZY)
	private DeliveryCorp defaultDeliveryCorp;

	/** 支付方式 */
	@ManyToMany(mappedBy = "shippingMethods", fetch = FetchType.LAZY)
	private Set<PaymentMethod> paymentMethods = new HashSet<PaymentMethod>();

	/** 订单 */
	@OneToMany(mappedBy = "shippingMethod", fetch = FetchType.LAZY)
	private Set<Order> orders = new HashSet<Order>();



}