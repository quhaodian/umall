/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 参数
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_parameter")
public class Parameter extends OrderEntity {

	private static final long serialVersionUID = -5833568086582136314L;

	/** 名称 */
	private String name;

	/** 参数组 */
	@ManyToOne(fetch = FetchType.LAZY)
	private ParameterGroup parameterGroup;



}