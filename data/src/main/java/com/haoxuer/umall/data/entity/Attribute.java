/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 属性
 * 
 * 
 * 
 */
@Entity
@Table(name = "bs_attribute")
public class Attribute extends OrderEntity {

	private static final long serialVersionUID = 2447794131117928367L;

	/** 名称 */
	private String name;

	/** 属性序号 */
	private Integer propertyIndex;

	/** 绑定分类 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false, updatable = false)
	private ProductCategory productCategory;

	/** 可选项 */
	@ElementCollection
	@CollectionTable(name = "bs_attribute_option")
	private List<String> options = new ArrayList<String>();

}