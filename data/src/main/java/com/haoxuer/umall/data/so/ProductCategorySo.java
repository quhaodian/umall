package com.haoxuer.umall.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年07月01日22:34:37.
*/
public class ProductCategorySo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
