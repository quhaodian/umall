package com.haoxuer.umall.data.dao.impl;

import com.haoxuer.umall.data.dao.ProductCategoryDao;
import com.haoxuer.umall.data.entity.ProductCategory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2019年07月01日22:34:37.
*/
@Repository

public class ProductCategoryDaoImpl extends CatalogDaoImpl<ProductCategory, Integer> implements ProductCategoryDao {

	@Override
	public ProductCategory findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductCategory save(ProductCategory bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public ProductCategory deleteById(Integer id) {
		ProductCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductCategory> getEntityClass() {
		return ProductCategory.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}