/*
 *
 *
 *
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.CatalogEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 商品分类
 */
@Data
@Entity
@Table(name = "bs_product_category")
public class ProductCategory extends CatalogEntity {


    /**
     * 上级分类
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCategory parent;

    /**
     * 下级分类
     */
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    @OrderBy("order asc")
    private Set<ProductCategory> children = new HashSet<ProductCategory>();

    /**
     * 商品
     */
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private Set<Product> products = new HashSet<Product>();

    /**
     * 筛选品牌
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "bs_product_category_brand")
    @OrderBy("order asc")
    private Set<Brand> brands = new HashSet<Brand>();

    /**
     * 参数组
     */
    @OneToMany(mappedBy = "productCategory", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @OrderBy("order asc")
    private Set<ParameterGroup> parameterGroups = new HashSet<ParameterGroup>();

    /**
     * 筛选属性
     */
    @OneToMany(mappedBy = "productCategory", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @OrderBy("order asc")
    private Set<Attribute> attributes = new HashSet<Attribute>();

    /**
     * 促销
     */
    @ManyToMany(mappedBy = "productCategories", fetch = FetchType.LAZY)
    private Set<Promotion> promotions = new HashSet<Promotion>();


    @Override
    public Integer getParentId() {
        if (parent != null) {
            return parent.getId();
        }
        return null;
    }
}