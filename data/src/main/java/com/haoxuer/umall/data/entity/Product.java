/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.annotations.ColType;
import com.haoxuer.discover.data.annotations.FormFieldAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Entity - 商品
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_product")
public class Product extends AbstractEntity {

	private static final long serialVersionUID = 2167830430439593293L;

	/** 点击数缓存名称 */
	public static final String HITS_CACHE_NAME = "productHits";

	/** 点击数缓存更新间隔时间 */
	public static final int HITS_CACHE_INTERVAL = 600000;

	/** 商品属性值属性个数 */
	public static final int ATTRIBUTE_VALUE_PROPERTY_COUNT = 20;

	/** 商品属性值属性名称前缀 */
	public static final String ATTRIBUTE_VALUE_PROPERTY_NAME_PREFIX = "attributeValue";

	/** 全称规格前缀 */
	public static final String FULL_NAME_SPECIFICATION_PREFIX = "[";

	/** 全称规格后缀 */
	public static final String FULL_NAME_SPECIFICATION_SUFFIX = "]";

	/** 全称规格分隔符 */
	public static final String FULL_NAME_SPECIFICATION_SEPARATOR = " ";

	/** 静态路径 */
	private static String staticPath;

	/**
	 * 排序类型
	 */
	public enum OrderType {

		/** 置顶降序 */
		topDesc,

		/** 价格升序 */
		priceAsc,

		/** 价格降序 */
		priceDesc,

		/** 销量降序 */
		salesDesc,

		/** 评分降序 */
		scoreDesc,

		/** 日期降序 */
		dateDesc
	}

	/** 编号 */
	@FormFieldAnnotation(title = "编号", sortNum = "2", grid = false, col = ColType.col_1)
	private String sn;

	/** 名称 */
	@FormFieldAnnotation(title = "商品名称", sortNum = "1", grid = true, col = ColType.col_2)
	private String name;

	/** 全称 */
	private String fullName;

	/** 销售价 */
	@FormFieldAnnotation(title = "商品价格", sortNum = "3", grid = true, col = ColType.col_1)
	private BigDecimal price;

	/** 成本价 */
	private BigDecimal cost;

	/** 市场价 */
	private BigDecimal marketPrice;

	/** 展示图片 */
	@FormFieldAnnotation(title = "商品封面", sortNum = "2", grid = false, col = ColType.col_1)
	private String image;

	/** 单位 */
	private String unit;

	/** 重量 */
	private Integer weight;

	/** 库存 */
	private Integer stock;

	/** 已分配库存 */
	private Integer allocatedStock;

	/** 库存备注 */
	private String stockMemo;

	/** 赠送积分 */
	private Long point;

	/** 是否上架 */
	private Boolean isMarketable;

	/** 是否列出 */
	private Boolean isList;

	/** 是否置顶 */
	private Boolean isTop;

	/** 是否为赠品 */
	private Boolean isGift;

	/** 介绍 */
	private String introduction;

	/** 备注 */
	private String memo;

	/** 搜索关键词 */
	private String keyword;

	/** 页面标题 */
	private String seoTitle;

	/** 页面关键词 */
	private String seoKeywords;

	/** 页面描述 */
	private String seoDescription;

	/** 评分 */
	private Float score;

	/** 总评分 */
	private Long totalScore;

	/** 评分数 */
	private Long scoreCount;

	/** 点击数 */
	private Long hits;

	/** 周点击数 */
	private Long weekHits;

	/** 月点击数 */
	private Long monthHits;

	/** 销量 */
	private Long sales;

	/** 周销量 */
	private Long weekSales;

	/** 月销量 */
	private Long monthSales;

	/** 周点击数更新日期 */
	private Date weekHitsDate;

	/** 月点击数更新日期 */
	private Date monthHitsDate;

	/** 周销量更新日期 */
	private Date weekSalesDate;

	/** 月销量更新日期 */
	private Date monthSalesDate;

	/** 商品属性值0 */
	private String attributeValue0;

	/** 商品属性值1 */
	private String attributeValue1;



	/** 商品分类 */
	@FormFieldAnnotation(title = "商品分类", sortNum = "2", grid = true, col = ColType.col_1)
	@ManyToOne(fetch = FetchType.LAZY)
	private ProductCategory category;

	/** 货品 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Goods goods;

	/** 品牌 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Brand brand;

	/** 商品图片 */
	@ElementCollection
	@CollectionTable(name = "bs_product_product_image")
	private List<ProductImage> productImages = new ArrayList<ProductImage>();

	/** 评论 */
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Review> reviews = new HashSet<Review>();

	/** 咨询 */
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Consultation> consultations = new HashSet<Consultation>();

	/** 标签 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_product_tag")
	@OrderBy("order asc")
	private Set<Tag> tags = new HashSet<Tag>();

	/** 收藏会员 */
	@ManyToMany(mappedBy = "favoriteProducts", fetch = FetchType.LAZY)
	private Set<Member> favoriteMembers = new HashSet<Member>();

//	/** 规格 */
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "bs_product_specification")
//	private Set<Specification> specifications = new HashSet<Specification>();
//
//	/** 规格值 */
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "bs_product_specification_value")
//	private Set<SpecificationValue> specificationValues = new HashSet<SpecificationValue>();

	/** 促销 */
	@ManyToMany(mappedBy = "products", fetch = FetchType.LAZY)
	private Set<Promotion> promotions = new HashSet<Promotion>();

	/** 购物车项 */
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<CartItem> cartItems = new HashSet<CartItem>();

	/** 订单项 */
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private Set<OrderItem> orderItems = new HashSet<OrderItem>();

	/** 赠品项 */
	@OneToMany(mappedBy = "gift", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<GiftItem> giftItems = new HashSet<GiftItem>();

	/** 到货通知 */
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<ProductNotify> productNotifies = new HashSet<ProductNotify>();

	/** 会员价 */
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "bs_product_member_price")
	private Map<MemberRank, BigDecimal> memberPrice = new HashMap<MemberRank, BigDecimal>();

	/** 参数值*/
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "bs_product_parameter_value")
	private Map<String, String> parameterValue = new HashMap<String, String>();



}