package com.haoxuer.umall.data.service;

import com.haoxuer.umall.data.entity.ProductCategory;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年07月01日22:34:37.
*/
public interface ProductCategoryService {

	ProductCategory findById(Integer id);

	ProductCategory save(ProductCategory bean);

	ProductCategory update(ProductCategory bean);

	ProductCategory deleteById(Integer id);
	
	ProductCategory[] deleteByIds(Integer[] ids);
	
	Page<ProductCategory> page(Pageable pageable);
	
	Page<ProductCategory> page(Pageable pageable, Object search);

	List<ProductCategory> findByTops(Integer pid);


    List<ProductCategory> child(Integer id,Integer max);

	List<ProductCategory> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}