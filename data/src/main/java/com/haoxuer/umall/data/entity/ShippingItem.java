/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 发货项
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_shipping_item")
public class ShippingItem extends AbstractEntity {

	private static final long serialVersionUID = 2756395514949325790L;

	/** 商品编号 */
	private String sn;

	/** 商品名称 */
	private String name;

	/** 数量 */
	private Integer quantity;

	/** 发货单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Shipping shipping;



}