/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 收货地址
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_receiver")
public class Receiver extends AbstractEntity {

	private static final long serialVersionUID = 2673602067029665976L;

	/** 收货地址最大保存数 */
	public static final Integer MAX_RECEIVER_COUNT = 8;

	/** 收货人 */
	private String consignee;

	/** 地区名称 */
	private String areaName;

	/** 地址 */
	private String address;

	/** 邮编 */
	private String zipCode;

	/** 电话 */
	private String phone;

	/** 是否默认 */
	private Boolean isDefault;

	/** 地区 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Area area;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;


}