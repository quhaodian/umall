package com.haoxuer.umall.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import com.haoxuer.umall.data.entity.Product;

/**
* Created by imake on 2019年07月01日22:45:27.
*/
public interface ProductDao extends BaseDao<Product,Long>{

	 Product findById(Long id);

	 Product save(Product bean);

	 Product updateByUpdater(Updater<Product> updater);

	 Product deleteById(Long id);
}