/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 消息
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_message")
public class Message extends AbstractEntity {

	private static final long serialVersionUID = -5035343536762850722L;

	/** 标题 */
	private String title;

	/** 内容 */
	private String content;

	/** ip */
	private String ip;

	/** 是否为草稿 */
	private Boolean isDraft;

	/** 发件人已读 */
	private Boolean senderRead;

	/** 收件人已读 */
	private Boolean receiverRead;

	/** 发件人删除 */
	private Boolean senderDelete;

	/** 收件人删除 */
	private Boolean receiverDelete;

	/** 发件人 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member sender;

	/** 收件人 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member receiver;

	/** 原消息 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Message forMessage;

	/** 回复消息 */
	@OneToMany(mappedBy = "forMessage", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy(value = "createDate asc")
	private Set<Message> replyMessages = new HashSet<Message>();


}