/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 咨询
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_consultation")
public class Consultation extends AbstractEntity {


	/** 访问路径前缀 */
	private static final String PATH_PREFIX = "/consultation/content";

	/** 访问路径后缀 */
	private static final String PATH_SUFFIX = ".jhtml";

	/** 内容 */
	private String content;

	/** 是否显示 */
	private Boolean isShow;

	/** IP */
	private String ip;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 商品 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Product product;

	/** 咨询 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Consultation forConsultation;

	/** 回复 */
	@OneToMany(mappedBy = "forConsultation", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("createDate asc")
	private Set<Consultation> replyConsultations = new HashSet<Consultation>();



}