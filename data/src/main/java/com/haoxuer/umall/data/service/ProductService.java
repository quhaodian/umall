package com.haoxuer.umall.data.service;

import com.haoxuer.umall.data.entity.Product;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年07月01日22:45:27.
*/
public interface ProductService {

	Product findById(Long id);

	Product save(Product bean);

	Product update(Product bean);

	Product deleteById(Long id);
	
	Product[] deleteByIds(Long[] ids);
	
	Page<Product> page(Pageable pageable);
	
	Page<Product> page(Pageable pageable, Object search);


	List<Product> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}