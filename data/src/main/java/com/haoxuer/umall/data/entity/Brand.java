/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 品牌
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_brand")
public class Brand extends OrderEntity {

	private static final long serialVersionUID = -6109590619136943215L;

	/** 访问路径前缀 */
	private static final String PATH_PREFIX = "/brand/content";

	/** 访问路径后缀 */
	private static final String PATH_SUFFIX = ".jhtml";

	/**
	 * 类型
	 */
	public enum Type {

		/** 文本 */
		text,

		/** 图片 */
		image
	}

	/** 名称 */
	private String name;

	/** 类型 */
	private Type type;

	/** logo */
	private String logo;

	/** 网址 */
	private String url;

	/** 介绍 */
	private String introduction;

	/** 商品 */
	@OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
	private Set<Product> products = new HashSet<Product>();

	/** 商品分类 */
	@ManyToMany(mappedBy = "brands", fetch = FetchType.LAZY)
	@OrderBy("order asc")
	private Set<ProductCategory> productCategories = new HashSet<ProductCategory>();

	/** 促销 */
	@ManyToMany(mappedBy = "brands", fetch = FetchType.LAZY)
	private Set<Promotion> promotions = new HashSet<Promotion>();



}