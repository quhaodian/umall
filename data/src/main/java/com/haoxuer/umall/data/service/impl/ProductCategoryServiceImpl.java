package com.haoxuer.umall.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.umall.data.dao.ProductCategoryDao;
import com.haoxuer.umall.data.entity.ProductCategory;
import com.haoxuer.umall.data.service.ProductCategoryService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2019年07月01日22:34:37.
*/


@Scope("prototype")
@Service
@Transactional
public class ProductCategoryServiceImpl implements ProductCategoryService {

	private ProductCategoryDao dao;


	@Override
	@Transactional(readOnly = true)
	public ProductCategory findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public List<ProductCategory> findByTops(Integer pid) {
		LinkedList<ProductCategory> result = new LinkedList<ProductCategory>();
		ProductCategory catalog = dao.findById(pid);
	    if(catalog != null){
			while ( catalog != null && catalog.getParent() != null ) {
				result.addFirst(catalog);
				catalog = dao.findById(catalog.getParentId());
			}
			result.addFirst(catalog);
	    }
		return result;
	}


    @Override
    public List<ProductCategory> child(Integer id,Integer max) {
        List<Order> orders=new ArrayList<Order>();
        orders.add(Order.asc("code"));
        List<Filter> list=new ArrayList<Filter>();
        list.add(Filter.eq("parent.id",id));
        return dao.list(0,max,list,orders);
	}

	@Override
    @Transactional
	public ProductCategory save(ProductCategory bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ProductCategory update(ProductCategory bean) {
		Updater<ProductCategory> updater = new Updater<ProductCategory>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ProductCategory deleteById(Integer id) {
		ProductCategory bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ProductCategory[] deleteByIds(Integer[] ids) {
		ProductCategory[] beans = new ProductCategory[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ProductCategoryDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ProductCategory> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ProductCategory> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ProductCategory> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}