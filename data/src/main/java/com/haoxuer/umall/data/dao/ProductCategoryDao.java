package com.haoxuer.umall.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import com.haoxuer.umall.data.entity.ProductCategory;

/**
* Created by imake on 2019年07月01日22:34:37.
*/
public interface ProductCategoryDao extends BaseDao<ProductCategory,Integer>{

	 ProductCategory findById(Integer id);

	 ProductCategory save(ProductCategory bean);

	 ProductCategory updateByUpdater(Updater<ProductCategory> updater);

	 ProductCategory deleteById(Integer id);
}