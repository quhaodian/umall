/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - SEO设置
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_seo")
public class Seo extends AbstractEntity {

	private static final long serialVersionUID = -3503657242384822672L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 首页 */
		index,

		/** 商品列表 */
		productList,

		/** 商品搜索 */
		productSearch,

		/** 商品页 */
		productContent,

		/** 文章列表 */
		articleList,

		/** 文章搜索 */
		articleSearch,

		/** 文章页 */
		articleContent,

		/** 品牌列表 */
		brandList,

		/** 品牌页 */
		brandContent
	}

	/** 类型 */
	private Type type;

	/** 页面标题 */
	private String title;

	/** 页面关键词 */
	private String keywords;

	/** 页面描述 */
	private String description;



}