/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 退货单
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_returns")
public class Returns extends AbstractEntity {


	/** 编号 */
	private String sn;

	/** 配送方式 */
	private String shippingMethod;

	/** 物流公司 */
	private String deliveryCorp;

	/** 运单号 */
	private String trackingNo;

	/** 物流费用 */
	private BigDecimal freight;

	/** 发货人 */
	private String shipper;

	/** 地区 */
	private String area;

	/** 地址 */
	private String address;

	/** 邮编 */
	private String zipCode;

	/** 电话 */
	private String phone;

	/** 操作员 */
	private String operator;

	/** 备注 */
	private String memo;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	/** 退货项 */
	@OneToMany(mappedBy = "returns", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ReturnsItem> returnsItems = new ArrayList<ReturnsItem>();


}