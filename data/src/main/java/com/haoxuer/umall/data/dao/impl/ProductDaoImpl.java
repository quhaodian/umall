package com.haoxuer.umall.data.dao.impl;

import com.haoxuer.umall.data.entity.Product;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.umall.data.dao.ProductDao;

/**
* Created by imake on 2019年07月01日22:45:27.
*/
@Repository

public class ProductDaoImpl extends CriteriaDaoImpl<Product, Long> implements ProductDao {

	@Override
	public Product findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Product save(Product bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Product deleteById(Long id) {
		Product entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Product> getEntityClass() {
		return Product.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}