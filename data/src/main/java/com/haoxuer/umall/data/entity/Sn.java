/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 序列号
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_sn")
public class Sn extends AbstractEntity {

	/**
	 * 类型
	 */
	public enum Type {

		/** 商品 */
		product,

		/** 订单 */
		order,

		/** 收款单 */
		payment,

		/** 退款单 */
		refunds,

		/** 发货单 */
		shipping,

		/** 退货单 */
		returns
	}

	/** 类型 */
	private Type type;

	/** 末值 */
	private Long lastValue;


}