/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Entity - 规格值
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_specification_value")
public class SpecificationValue extends OrderEntity {

	private static final long serialVersionUID = -8624741017444123488L;

	/** 名称 */
	private String name;

	/** 图片 */
	private String image;

	/** 规格 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Specification specification;

	/** 商品
	@ManyToMany(mappedBy = "specificationValues", fetch = FetchType.LAZY)
	private Set<Product> products = new HashSet<Product>();
	 */


}