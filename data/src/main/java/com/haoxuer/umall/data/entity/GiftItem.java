/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;

/**
 * Entity - 赠品项
 * 
 * 
 * 
 */
//, uniqueConstraints = { @UniqueConstraint(columnNames = { "gift", "promotion" }) }
@Data
@Entity
@Table(name = "bs_gift_item")
public class GiftItem extends AbstractEntity {

	private static final long serialVersionUID = 6593657730952481829L;

	/** 数量 */
	private Integer quantity;

	/** 赠品 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Product gift;

	/** 促销 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Promotion promotion;



}