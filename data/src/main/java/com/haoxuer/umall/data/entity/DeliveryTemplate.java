/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 快递单模板
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_delivery_template")
public class DeliveryTemplate extends AbstractEntity {

	private static final long serialVersionUID = -3711024981692804054L;

	/** 名称 */
	private String name;

	/** 内容 */
	private String content;

	/** 宽度 */
	private Integer width;

	/** 高度 */
	private Integer height;

	/** 偏移量X */
	private Integer offsetX;

	/** 偏移量Y */
	private Integer offsetY;

	/** 背景图 */
	private String background;

	/** 是否默认 */
	private Boolean isDefault;

	/** 备注 */
	private String memo;



}