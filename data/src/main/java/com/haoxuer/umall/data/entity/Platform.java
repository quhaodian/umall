package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.annotations.ColType;
import com.haoxuer.discover.data.annotations.FormAnnotation;
import com.haoxuer.discover.data.annotations.FormFieldAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@FormAnnotation(title = "平台", add = "添加平台", list = "平台", update = "更新平台")
@Entity
@Table(name = "bs_platform")
public class Platform extends AbstractEntity {

  @FormFieldAnnotation(title = "平台名称", sortNum = "5", grid = true, col = ColType.col_2)
  private String name;

  @FormFieldAnnotation(title = "平台介绍", sortNum = "5", grid = true, col = ColType.col_8)
  private String note;

}
