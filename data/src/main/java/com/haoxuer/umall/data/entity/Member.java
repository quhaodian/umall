package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.annotations.ColType;
import com.haoxuer.discover.data.annotations.FormAnnotation;
import com.haoxuer.discover.data.annotations.FormFieldAnnotation;
import com.haoxuer.discover.user.data.entity.AbstractUser;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 网站用户
 */

@Data
@FormAnnotation(title = "用户", add = "添加用户", list = "用户", update = "更新用户")
@Entity
@Table(name = "user_info")
public class Member extends AbstractUser {

  /**
   * 性别
   */
  public enum Gender {

    /** 男 */
    male,

    /** 女 */
    female
  }


  @FormFieldAnnotation(title = "个人介绍", sortNum = "2", grid = true, col = ColType.col_2)
  private String note;
  
  private String edge;
  
  @FormFieldAnnotation(title = "性别", sortNum = "2", grid = true, col = ColType.col_1)
  @Column(length = 5)
  private String sex;


  
  
  /**
   * 个人介绍
   */
  private String introduce;
  
  @Column(length = 20)
  private String job;
  
  @Column(length = 30)
  private String companyName;

  
  


  /** E-mail */
  private String email;

  /** 积分 */
  private Long point;

  /** 消费金额 */
  private BigDecimal amount;

  /** 余额 */
  private BigDecimal balance;

  /** 是否启用 */
  private Boolean isEnabled;

  /** 是否锁定 */
  private Boolean isLocked;

  /** 连续登录失败次数 */
  private Integer loginFailureCount;

  /** 锁定日期 */
  private Date lockedDate;

  /** 注册IP */
  private String registerIp;

  /** 最后登录IP */
  private String loginIp;

  /** 最后登录日期 */
  private Date loginDate;

  /** 姓名 */
  private String name;

  /** 性别 */
  private Gender gender;

  /** 出生日期 */
  private Date birth;

  /** 地址 */
  private String address;

  /** 邮编 */
  private String zipCode;

  /** 电话 */
  private String phone;

  /** 手机 */
  private String mobile;



  /** 安全密匙 */
  @Embedded
  private SafeKey safeKey;


  /** 会员等级 */
  @ManyToOne(fetch = FetchType.LAZY)
  private MemberRank memberRank;

  /** 购物车 */
  @ManyToOne(fetch = FetchType.LAZY)
  private Cart cart;

  /** 订单 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<Order> orders = new HashSet<Order>();

  /** 预存款 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<Deposit> deposits = new HashSet<Deposit>();

  /** 收款单 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<Payment> payments = new HashSet<Payment>();

  /** 优惠码 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<CouponCode> couponCodes = new HashSet<CouponCode>();

  /** 收货地址 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @OrderBy("isDefault desc, createDate desc")
  private Set<Receiver> receivers = new HashSet<Receiver>();

  /** 评论 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @OrderBy("createDate desc")
  private Set<Review> reviews = new HashSet<Review>();

  /** 咨询 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @OrderBy("createDate desc")
  private Set<Consultation> consultations = new HashSet<Consultation>();

  /** 收藏商品 */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "bs_member_favorite_product")
  @OrderBy("createDate desc")
  private Set<Product> favoriteProducts = new HashSet<Product>();

  /** 到货通知 */
  @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<ProductNotify> productNotifies = new HashSet<ProductNotify>();

  /** 接收的消息 */
  @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<Message> inMessages = new HashSet<Message>();

  /** 发送的消息 */
  @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  private Set<Message> outMessages = new HashSet<Message>();
  

}
