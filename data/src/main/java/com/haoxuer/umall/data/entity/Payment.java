/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Entity - 收款单
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_payment")
public class Payment extends AbstractEntity {

	private static final long serialVersionUID = -5052430116564638634L;

	/** 支付方式分隔符 */
	public static final String PAYMENT_METHOD_SEPARATOR = " - ";

	/**
	 * 类型
	 */
	public enum Type {

		/** 订单支付 */
		payment,

		/** 预存款充值 */
		recharge
	}

	/**
	 * 方式
	 */
	public enum Method {

		/** 在线支付 */
		online,

		/** 线下支付 */
		offline,

		/** 预存款支付 */
		deposit
	}

	/**
	 * 状态
	 */
	public enum Status {

		/** 等待支付 */
		wait,

		/** 支付成功 */
		success,

		/** 支付失败 */
		failure
	}

	/** 编号 */
	private String sn;

	/** 类型 */
	private Type type;

	/** 方式 */
	private Method method;

	/** 状态 */
	private Status status;

	/** 支付方式 */
	private String paymentMethod;

	/** 收款银行 */
	private String bank;

	/** 收款账号 */
	private String account;

	/** 支付手续费 */
	private BigDecimal fee;

	/** 付款金额 */
	private BigDecimal amount;

	/** 付款人 */
	private String payer;

	/** 操作员 */
	private String operator;

	/** 付款日期 */
	private Date paymentDate;

	/** 备注 */
	private String memo;

	/** 支付插件ID */
	private String paymentPluginId;

	/** 到期时间 */
	private Date expire;

	/** 预存款 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Deposit deposit;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;


}