/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Entity - 促销
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_promotion")
public class Promotion extends OrderEntity {

	private static final long serialVersionUID = 3536993535267962279L;

	/** 访问路径前缀 */
	private static final String PATH_PREFIX = "/promotion/content";

	/** 访问路径后缀 */
	private static final String PATH_SUFFIX = ".jhtml";

	/** 名称 */
	private String name;

	/** 标题 */
	private String title;

	/** 起始日期 */
	private Date beginDate;

	/** 结束日期 */
	private Date endDate;

	/** 最小商品数量 */
	private Integer minimumQuantity;

	/** 最大商品数量 */
	private Integer maximumQuantity;

	/** 最小商品价格 */
	private BigDecimal minimumPrice;

	/** 最大商品价格 */
	private BigDecimal maximumPrice;

	/** 价格运算表达式 */
	private String priceExpression;

	/** 积分运算表达式 */
	private String pointExpression;

	/** 是否免运费 */
	private Boolean isFreeShipping;

	/** 是否允许使用优惠券 */
	private Boolean isCouponAllowed;

	/** 介绍 */
	private String introduction;

	/** 允许参加会员等级 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_promotion_member_rank")
	private Set<MemberRank> memberRanks = new HashSet<MemberRank>();

	/** 允许参与商品分类 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_promotion_product_category")
	private Set<ProductCategory> productCategories = new HashSet<ProductCategory>();

	/** 允许参与商品 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_promotion_product")
	private Set<Product> products = new HashSet<Product>();

	/** 允许参与品牌 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_promotion_brand")
	private Set<Brand> brands = new HashSet<Brand>();

	/** 赠送优惠券 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_promotion_coupon")
	private Set<Coupon> coupons = new HashSet<Coupon>();

	/** 赠品 */
	@OneToMany(mappedBy = "promotion", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<GiftItem> giftItems = new ArrayList<GiftItem>();



}