/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 支付方式
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_payment_method")
public class PaymentMethod extends OrderEntity {

	private static final long serialVersionUID = 6949816500116581199L;

	/**
	 * 方式
	 */
	public enum Method {

		/** 在线支付 */
		online,

		/** 线下支付 */
		offline
	};

	/** 名称 */
	private String name;

	/** 方式 */
	private Method method;

	/** 超时时间 */
	private Integer timeout;

	/** 图标 */
	private String icon;

	/** 介绍 */
	private String description;

	/** 内容 */
	private String content;

	/** 支持配送方式 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bs_payment_shipping_method")
	@OrderBy("order asc")
	private Set<ShippingMethod> shippingMethods = new HashSet<ShippingMethod>();

	/** 订单 */
	@OneToMany(mappedBy = "paymentMethod", fetch = FetchType.LAZY)
	private Set<Order> orders = new HashSet<Order>();



}