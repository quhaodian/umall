/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Entity - 商品图片
 * 
 * 
 * 
 */
@Data
@Embeddable
public class ProductImage implements Serializable, Comparable<ProductImage> {

	private static final long serialVersionUID = -673883300094536107L;

	/** 标题 */
	private String title;

	/** 原图片 */
	private String source;

	/** 大图片 */
	private String large;

	/** 中图片 */
	private String medium;

	/** 缩略图 */
	private String thumbnail;

	/** 排序 */
	@Column(name = "orderNum")
	private Integer order;

	/** 文件 */
	@Transient
	private MultipartFile file;



	/**
	 * 实现compareTo方法
	 *
	 * @param productImage
	 *            商品图片
	 * @return 比较结果
	 */
	public int compareTo(ProductImage productImage) {
		return new CompareToBuilder().append(getOrder(), productImage.getOrder()).toComparison();
	}

}