/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Entity - 优惠券
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_coupon")
public class Coupon extends AbstractEntity {

	private static final long serialVersionUID = -7907808728349149722L;

	/** 名称 */
	private String name;

	/** 前缀 */
	private String prefix;

	/** 使用起始日期 */
	private Date beginDate;

	/** 使用结束日期 */
	private Date endDate;

	/** 最小商品数量 */
	private Integer minimumQuantity;

	/** 最大商品数量 */
	private Integer maximumQuantity;

	/** 最小商品价格 */
	private BigDecimal minimumPrice;

	/** 最大商品价格 */
	private BigDecimal maximumPrice;

	/** 价格运算表达式 */
	private String priceExpression;

	/** 是否启用 */
	private Boolean isEnabled;

	/** 是否允许积分兑换 */
	private Boolean isExchange;

	/** 积分兑换数 */
	private Long point;

	/** 介绍 */
	private String introduction;

	/** 优惠码 */
	@OneToMany(mappedBy = "coupon", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<CouponCode> couponCodes = new HashSet<CouponCode>();

	/** 促销 */
	@ManyToMany(mappedBy = "coupons", fetch = FetchType.LAZY)
	private Set<Promotion> promotions = new HashSet<Promotion>();

	/** 订单 */
	@ManyToMany(mappedBy = "coupons", fetch = FetchType.LAZY)
	private List<Order> orders = new ArrayList<Order>();


}