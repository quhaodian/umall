package com.haoxuer.umall.data.dao;


import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.umall.data.entity.Member;

public interface MemberDao extends CriteriaDao<Member, Long> {
  
   Member findById(Long id);
  
   Member save(Member bean);
  
   Member updateByUpdater(Updater<Member> updater);
  
   Member deleteById(Long id);
}