/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 到货通知
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_product_notify")
public class ProductNotify extends AbstractEntity {

	private static final long serialVersionUID = 3192904068727393421L;

	/** E-mail */
	private String email;

	/** 是否已发送 */
	private Boolean hasSent;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 商品 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Product product;


}