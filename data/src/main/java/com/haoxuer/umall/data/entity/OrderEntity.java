/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.CompareToBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Entity - 排序基类
 * 
 * 
 * 
 */
@Data
@MappedSuperclass
public abstract class OrderEntity extends AbstractEntity implements Comparable<OrderEntity> {

	private static final long serialVersionUID = 5995013015967525827L;

	/** "排序"属性名称 */
	public static final String ORDER_PROPERTY_NAME = "order";

	/** 排序 */
	@Column(name = "orderNum")
	private Integer order;


	public int compareTo(OrderEntity orderEntity) {
		return new CompareToBuilder().append(getOrder(), orderEntity.getOrder()).append(getId(), orderEntity.getId()).toComparison();
	}
}