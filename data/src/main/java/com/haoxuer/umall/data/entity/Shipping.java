/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 发货单
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_shipping")
public class Shipping extends AbstractEntity {

	private static final long serialVersionUID = -261737051893669935L;

	/** 编号 */
	private String sn;

	/** 配送方式 */
	private String shippingMethod;

	/** 物流公司 */
	private String deliveryCorp;

	/** 物流公司网址 */
	private String deliveryCorpUrl;

	/** 物流公司代码 */
	private String deliveryCorpCode;

	/** 运单号 */
	private String trackingNo;

	/** 物流费用 */
	private BigDecimal freight;

	/** 收货人 */
	private String consignee;

	/** 地区 */
	private String area;

	/** 地址 */
	private String address;

	/** 邮编 */
	private String zipCode;

	/** 电话 */
	private String phone;

	/** 操作员 */
	private String operator;

	/** 备注 */
	private String memo;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	/** 发货项 */
	@OneToMany(mappedBy = "shipping", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ShippingItem> shippingItems = new ArrayList<ShippingItem>();


}