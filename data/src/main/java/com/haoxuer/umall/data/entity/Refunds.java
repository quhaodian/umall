/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Entity - 退款单
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_refunds")
public class Refunds extends AbstractEntity {

	private static final long serialVersionUID = 354885216604823632L;

	/**
	 * 方式
	 */
	public enum Method {

		/** 在线支付 */
		online,

		/** 线下支付 */
		offline,

		/** 预存款支付 */
		deposit
	}

	/** 编号 */
	private String sn;

	/** 方式 */
	private Method method;

	/** 支付方式 */
	private String paymentMethod;

	/** 退款银行 */
	private String bank;

	/** 退款账号 */
	private String account;

	/** 退款金额 */
	private BigDecimal amount;

	/** 收款人 */
	private String payee;

	/** 操作员 */
	private String operator;

	/** 备注 */
	private String memo;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;



}