/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 导航
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_navigation")
public class Navigation extends OrderEntity {

	private static final long serialVersionUID = -7635757647887646795L;

	/**
	 * 位置
	 */
	public enum Position {

		/** 顶部 */
		top,

		/** 中间 */
		middle,

		/** 底部 */
		bottom
	}

	/** 名称 */
	private String name;

	/** 位置 */
	private Position position;

	/** 链接地址 */
	private String url;

	/** 是否新窗口打开 */
	private Boolean isBlankTarget;



}