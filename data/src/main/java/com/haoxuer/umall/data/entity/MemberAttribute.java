/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 会员注册项
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_member_attribute")
public class MemberAttribute extends OrderEntity {

	private static final long serialVersionUID = 4513705276569738136L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 姓名 */
		name,

		/** 性别 */
		gender,

		/** 出生日期 */
		birth,

		/** 地区 */
		area,

		/** 地址 */
		address,

		/** 邮编 */
		zipCode,

		/** 电话 */
		phone,

		/** 手机 */
		mobile,

		/** 文本 */
		text,

		/** 单选项 */
		select,

		/** 多选项 */
		checkbox
	}

	/** 名称 */
	private String name;

	/** 类型 */
	private Type type;

	/** 是否启用 */
	private Boolean isEnabled;

	/** 是否必填 */
	private Boolean isRequired;

	/** 属性序号 */
	private Integer propertyIndex;

	/** 可选项 */
	@ElementCollection
	@CollectionTable(name = "bs_member_attribute_option")
	private List<String> options = new ArrayList<String>();



}