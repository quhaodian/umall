/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 物流公司
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_delivery_corp")
public class DeliveryCorp extends OrderEntity {

	private static final long serialVersionUID = 10595703086045998L;

	/** 名称 */
	private String name;

	/** 网址 */
	private String url;

	/** 代码 */
	private String code;

	/** 配送方式 */
	@OneToMany(mappedBy = "defaultDeliveryCorp", fetch = FetchType.LAZY)
	private Set<ShippingMethod> shippingMethods = new HashSet<ShippingMethod>();



}