/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * Entity - 安全密钥
 * 
 * 
 * 
 */
@Data
@Embeddable
public class SafeKey implements Serializable {


	/** 密钥 */
	@Column(name = "safe_key_value")
	private String value;

	/** 到期时间 */
	@Transient
	private Date expire;

}