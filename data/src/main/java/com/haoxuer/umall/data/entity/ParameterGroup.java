/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 参数组
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_parameter_group")
public class ParameterGroup extends OrderEntity {

	private static final long serialVersionUID = 192003501177471941L;

	/** 名称 */
	private String name;

	/** 绑定分类 */
	@ManyToOne(fetch = FetchType.LAZY)
	private ProductCategory productCategory;

	/** 参数 */
	@OneToMany(mappedBy = "parameterGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("order asc")
	private List<Parameter> parameters = new ArrayList<Parameter>();



}