/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 货品
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_goods")
public class Goods extends AbstractEntity {

	private static final long serialVersionUID = -6977025562650112419L;

	/** 商品 */
	@OneToMany(mappedBy = "goods", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Product> products = new HashSet<Product>();



}