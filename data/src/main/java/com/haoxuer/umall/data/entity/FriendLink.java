/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 友情链接
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_friend_link")
public class FriendLink extends OrderEntity {

	private static final long serialVersionUID = 3019642557500517628L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 文本 */
		text,

		/** 图片 */
		image
	}

	/** 名称 */
	private String name;

	/** 类型 */
	private Type type;

	/** logo */
	private String logo;

	/** 链接地址 */
	private String url;



}