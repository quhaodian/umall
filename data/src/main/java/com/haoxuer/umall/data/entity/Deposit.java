/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Entity - 预存款
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_deposit")
public class Deposit extends AbstractEntity {

	private static final long serialVersionUID = -8323452873046981882L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 会员充值 */
		memberRecharge,

		/** 会员支付 */
		memberPayment,

		/** 后台充值 */
		adminRecharge,

		/** 后台扣费 */
		adminChargeback,

		/** 后台支付 */
		adminPayment,

		/** 后台退款 */
		adminRefunds
	}

	/** 类型 */
	private Type type;

	/** 收入金额 */
	private BigDecimal credit;

	/** 支出金额 */
	private BigDecimal debit;

	/** 当前余额 */
	private BigDecimal balance;

	/** 操作员 */
	private String operator;

	/** 备注 */
	private String memo;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	/** 收款单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Payment payment;



}