/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Entity - 优惠码
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_coupon_code")
public class CouponCode extends AbstractEntity {

	private static final long serialVersionUID = -1812874037224306719L;

	/** 号码 */
	private String code;

	/** 是否已使用 */
	private Boolean isUsed;

	/** 使用日期 */
	private Date usedDate;

	/** 优惠券 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Coupon coupon;

	/** 会员 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Member member;

	/** 订单 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;



}