/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity - 发货点
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_delivery_center")
public class DeliveryCenter extends AbstractEntity {

	private static final long serialVersionUID = 3328996121729039075L;

	/** 名称 */
	private String name;

	/** 联系人 */
	private String contact;

	/** 地区名称 */
	private String areaName;

	/** 地址 */
	private String address;

	/** 邮编 */
	private String zipCode;

	/** 电话 */
	private String phone;

	/** 手机 */
	private String mobile;

	/** 备注 */
	private String memo;

	/** 是否默认 */
	private Boolean isDefault;

	/** 地区 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Area area;



}