/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity - 标签
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_tag")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "xx_tag_sequence")
public class Tag extends OrderEntity {

	private static final long serialVersionUID = -2735037966597250149L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 文章标签 */
		article,

		/** 商品标签 */
		product
	};

	/** 名称 */
	private String name;

	/** 类型 */
	@Column(name = "tagType")
	private Type type;

	/** 图标 */
	private String icon;

	/** 备注 */
	private String memo;

	/** 文章 */
	@ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
	private Set<Article> articles = new HashSet<Article>();

	/** 商品 */
	@ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
	private Set<Product> products = new HashSet<Product>();



}