/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity - 规格
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_specification")
public class Specification extends OrderEntity {

	private static final long serialVersionUID = -6346775052811140926L;

	/**
	 * 类型
	 */
	public enum Type {

		/** 文本 */
		text,

		/** 图片 */
		image
	};

	/** 名称 */
	private String name;

	/** 类型 */
	private Type type;

	/** 备注 */
	private String memo;

	/** 规格值 */
	@OneToMany(mappedBy = "specification", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("order asc")
	private List<SpecificationValue> specificationValues = new ArrayList<SpecificationValue>();

	/** 商品
	@ManyToMany(mappedBy = "specifications", fetch = FetchType.LAZY)
	private Set<Product> products = new HashSet<Product>();

	 */

}