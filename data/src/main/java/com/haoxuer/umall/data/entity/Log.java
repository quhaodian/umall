/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 日志
 * 
 * 
 * 
 */
@Data
@Entity
@Table(name = "bs_log")
public class Log extends AbstractEntity {

	private static final long serialVersionUID = -4494144902110236826L;

	/** "日志内容"属性名称 */
	public static final String LOG_CONTENT_ATTRIBUTE_NAME = Log.class.getName() + ".CONTENT";

	/** 操作 */
	private String operation;

	/** 操作员 */
	private String operator;

	/** 内容 */
	private String content;

	/** 请求参数 */
	private String parameter;

	/** IP */
	private String ip;



}