package com.haoxuer.umall.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.umall.api.domain.simple.CategorySimple;
import com.haoxuer.umall.data.entity.ProductCategory;

public class CategorySimpleConver implements Conver<CategorySimple, ProductCategory> {
    @Override
    public CategorySimple conver(ProductCategory source) {
        CategorySimple result = new CategorySimple();
        result.setId(source.getId());
        result.setName(source.getName());
        if (source.getParent() != null) {
            result.setPid(source.getParent().getId());
        } else {
            result.setPid(0);
        }
        return result;
    }
}
