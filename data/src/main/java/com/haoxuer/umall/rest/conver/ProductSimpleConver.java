package com.haoxuer.umall.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.umall.api.domain.simple.ProductSimple;
import com.haoxuer.umall.data.entity.Product;

public class ProductSimpleConver implements Conver<ProductSimple, Product> {
    @Override
    public ProductSimple conver(Product source) {
        ProductSimple result = new ProductSimple();
        result.setLogo(source.getImage());
        result.setName(source.getName());
        result.setId(source.getId());
        if (source.getCategory() != null) {
            result.setCatalog(source.getCategory().getId());
        }

        return result;
    }
}
