package com.haoxuer.umall.controller.rest;

import com.haoxuer.umall.api.apis.ProductApi;
import com.haoxuer.umall.api.domain.list.CategoryList;
import com.haoxuer.umall.api.domain.list.ProductList;
import com.haoxuer.umall.api.domain.page.ProductPage;
import com.haoxuer.umall.api.domain.request.PlatformRequest;
import com.haoxuer.umall.api.domain.request.ProductSearchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/rest")
@RestController
public class ProductRestController  {

    @RequestMapping("/product/all")
    public ProductList all(PlatformRequest request) {
        return api.all(request);
    }

    @RequestMapping("/product/search")
    public ProductPage search(ProductSearchRequest request) {
        return api.search(request);
    }

    @RequestMapping("/product/allCategory")
    public CategoryList allCategory(PlatformRequest request) {
        return api.allCategory(request);
    }

    @Autowired
    private ProductApi api;
}
