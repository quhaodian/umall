/*
 * 
 * 
 * 
 */
package com.haoxuer.umall.controller.admin;

import com.haoxuer.discover.plug.data.service.StorageService;
import com.haoxuer.discover.plug.data.vo.FileInfo;
import com.haoxuer.umall.data.vo.FileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller - 文件处理
 * 
 * 
 * 
 */
@Controller
@RequestMapping("/admin/file")
public class FileController   {

    @Autowired
	StorageService storageService;

	public String uploadLocal(FileInfo.FileType fileType, MultipartFile multipartFile) {
		return storageService.uploadLocal(fileType,multipartFile);
	}

	/**
	 * 上传
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public FileResponse upload(FileInfo.FileType fileType, MultipartFile file, HttpServletResponse response) {

        FileResponse result=new FileResponse();
		Map<String, Object> data = new HashMap<String, Object>();
		String url = storageService.upload(fileType, file);
        result.setUrl(url);
		return result;
	}


}