/**
 * @apiDefine 1user 1.0 用户模块
 */



/**
 * @api {post} /rest/user/loginoauth.htm 1.01 通过第三方登陆
 *
 * @apiVersion 0.0.1
 *
 * @apiName loginOauth
 *
 * @apiGroup 1user
 *
 * @apiParam {String} code 令牌
 *
 * @apiParam {String} type 第三方类型（wxapp）
 *
 * @apiPermission none
 *
 * @apiDescription 通过第三方登陆,要是没有用户信息，系统会创建一份用户信息
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 */



