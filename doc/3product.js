/**
 * @apiDefine 3product 3.0 商品模块
 */

/**
 * @api {post} /rest/product/allCategory.htm 3.01 所有分类
 *
 * @apiVersion 0.0.1
 *
 * @apiName allCategory
 *
 * @apiGroup 3product
 *
 * @apiParam {String} userToken 用户令牌
 *
 *
 * @apiPermission none
 *
 * @apiDescription 通过账号密码登陆
 *
 *
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {list} list  数据集合
 *
 * @apiSuccess {list} list.id  分类id
 *
 * @apiSuccess {list} list.name  分类名称
 *
 * @apiSuccess {list} list.pid  父id
 *
 */


/**
 * @api {post} /rest/product/all.htm 3.02 获取所有商品
 *
 * @apiVersion 0.0.1
 *
 * @apiName all
 *
 * @apiGroup 3product
 *
 * @apiParam {String} userToken 用户令牌
 *
 *
 * @apiPermission none
 *
 * @apiDescription 通过账号密码登陆
 *
 *
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {list} list  数据集合
 *
 * @apiSuccess {list} list.id  商品id
 *
 * @apiSuccess {list} list.name  商品名称
 *
 * @apiSuccess {list} list.catalog  商品分类id
 *
 * apiSuccess {list} list.logo  商品图片
 *
 */


/**
 * @api {post} /rest/product/search.htm 3.03 根据商品名称进行搜索
 *
 * @apiVersion 0.0.1
 *
 * @apiName search
 *
 * @apiGroup 3product
 *
 * @apiParam {String} userToken 用户令牌
 *
 * @apiParam {int} no 页码
 *
 * @apiParam {int} size 大小
 *
 * @apiParam {String} name 商品名称
 *
 *
 * @apiPermission none
 *
 * @apiDescription 通过账号密码登陆
 *
 *
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {list} list  数据集合
 *
 * @apiSuccess {list} list.id  商品id
 *
 * @apiSuccess {list} list.name  商品名称
 *
 * @apiSuccess {list} list.catalog  商品分类id
 *
 * apiSuccess {list} list.logo  商品图片
 *
 */