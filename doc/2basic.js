/**
 * @apiDefine 2basic 2.0 公共模块
 */

/**
 * @api {post} /rest/ad/page_by_position.htm 2.01 根据广告位id获取对应的广告
 *
 * @apiVersion 0.0.1
 *
 * @apiName page_by_position
 *
 * @apiGroup 2basic
 *
 * @apiParam {String} userToken 用户令牌
 *
 * @apiParam {int} id 广告位id (2:banner图)
 *
 * @apiParam {int} no 页码
 *
 * @apiParam {int} size 大小
 *
 *
 * @apiPermission none
 *
 * @apiDescription 通过账号密码登陆
 *
 *
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {list} list  数据集合
 *
 * @apiSuccess {list} list.id  广告id
 *
 * @apiSuccess {list} list.title  广告标题
 *
 * @apiSuccess {list} list.path  广告图片路径
 *
 * @apiSuccess {list} list.url  广告内容地址
 *
 *
 */



/**
 * @api {post} /storage/upload.htm 2.02 上传文件
 *
 * @apiVersion 0.0.1
 *
 * @apiName upload
 *
 * @apiGroup 2basic
 *
 * @apiParam {int} num 排序号
 *
 * @apiParam {file} file 文件
 *
 * @apiParam {String} userToken 用户令牌
 *
 * @apiPermission none
 *
 * @apiDescription 上传文件
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {string} url 网络地址
 *
 * @apiSuccess {string} num 排序号
 *
 *
 */


/**
 * @api {post} /rest/dictionary/findbyid.htm 2.03 数据字典接口(1性别,2年龄,3职业)
 *
 * @apiVersion 0.0.1
 *
 * @apiName findbyid
 *
 * @apiGroup 2basic
 *
 * @apiParam {int} id 字典id
 *
 * @apiParam {String} userToken 用户令牌
 *
 * @apiPermission none
 *
 * @apiDescription 数据字典接口
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {string} name 字典名称
 *
 * @apiSuccess {Long} version 字典版本
 *
 *
 * @apiSuccess {array} items 字典内容集合
 *
 */


/**
 * @api {post} /rest/ad/list.htm 2.04 获取所有广告
 *
 * @apiVersion 0.0.1
 *
 * @apiName list
 *
 * @apiGroup 2basic
 *
 * @apiParam {String} userToken 用户令牌
 *
 * @apiParam {int} no 页码
 *
 * @apiParam {int} size 大小
 *
 *
 * @apiPermission none
 *
 * @apiDescription 获取所有广告
 *
 *
 *
 *
 * @apiSuccess {Int} code 状态码(默认为0)
 *
 * @apiSuccess {string} msg 状态消息
 *
 * @apiSuccess {list} list  数据集合
 *
 * @apiSuccess {int} list.id  广告位id
 *
 * @apiSuccess {string} list.name  广告位名称
 *
 * @apiSuccess {list} list.ads  广告集合
 *
 * @apiSuccess {int} list.ads.id  广告id
 *
 * @apiSuccess {string} list.ads.title  广告标题
 *
 * @apiSuccess {string} list.ads.note  广告内容
 *
 * @apiSuccess {string} list.ads.bussId  业务id
 *
 * @apiSuccess {string} list.ads.url  跳转url
 *
 * @apiSuccess {list} list.ads.catalog  业务id类型（1不能点击，2景点 ，3游记 ，4任务）
 *
 *
 */
