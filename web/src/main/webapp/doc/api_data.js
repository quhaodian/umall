define({ "api": [
  {
    "type": "post",
    "url": "/rest/user/loginoauth.htm",
    "title": "1.01 通过第三方登陆",
    "version": "0.0.1",
    "name": "loginOauth",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>令牌</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>第三方类型（wxapp）</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过第三方登陆,要是没有用户信息，系统会创建一份用户信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/user/loginoauth.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/dictionary/findbyid.htm",
    "title": "2.03 数据字典接口(1性别,2年龄,3职业)",
    "version": "0.0.1",
    "name": "findbyid",
    "group": "2basic",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>字典id</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>数据字典接口</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>字典名称</p>"
          },
          {
            "group": "Success 200",
            "type": "Long",
            "optional": false,
            "field": "version",
            "description": "<p>字典版本</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "items",
            "description": "<p>字典内容集合</p>"
          }
        ]
      }
    },
    "filename": "./2basic.js",
    "groupTitle": "2.0 公共模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/dictionary/findbyid.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/ad/list.htm",
    "title": "2.04 获取所有广告",
    "version": "0.0.1",
    "name": "list",
    "group": "2basic",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>获取所有广告</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "list.id",
            "description": "<p>广告位id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "list.name",
            "description": "<p>广告位名称</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.ads",
            "description": "<p>广告集合</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "list.ads.id",
            "description": "<p>广告id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "list.ads.title",
            "description": "<p>广告标题</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "list.ads.note",
            "description": "<p>广告内容</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "list.ads.bussId",
            "description": "<p>业务id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "list.ads.url",
            "description": "<p>跳转url</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.ads.catalog",
            "description": "<p>业务id类型（1不能点击，2景点 ，3游记 ，4任务）</p>"
          }
        ]
      }
    },
    "filename": "./2basic.js",
    "groupTitle": "2.0 公共模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/ad/list.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/ad/page_by_position.htm",
    "title": "2.01 根据广告位id获取对应的广告",
    "version": "0.0.1",
    "name": "page_by_position",
    "group": "2basic",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>广告位id (2:banner图)</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过账号密码登陆</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.id",
            "description": "<p>广告id</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.title",
            "description": "<p>广告标题</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.path",
            "description": "<p>广告图片路径</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.url",
            "description": "<p>广告内容地址</p>"
          }
        ]
      }
    },
    "filename": "./2basic.js",
    "groupTitle": "2.0 公共模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/ad/page_by_position.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/storage/upload.htm",
    "title": "2.02 上传文件",
    "version": "0.0.1",
    "name": "upload",
    "group": "2basic",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "num",
            "description": "<p>排序号</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "file",
            "optional": false,
            "field": "file",
            "description": "<p>文件</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>上传文件</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>网络地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "num",
            "description": "<p>排序号</p>"
          }
        ]
      }
    },
    "filename": "./2basic.js",
    "groupTitle": "2.0 公共模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/storage/upload.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/product/all.htm",
    "title": "3.02 获取所有商品",
    "version": "0.0.1",
    "name": "all",
    "group": "3product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过账号密码登陆</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.id",
            "description": "<p>商品id</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.catalog",
            "description": "<p>商品分类id</p> <p>apiSuccess {list} list.logo  商品图片</p>"
          }
        ]
      }
    },
    "filename": "./3product.js",
    "groupTitle": "3.0 商品模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/product/all.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/product/allCategory.htm",
    "title": "3.01 所有分类",
    "version": "0.0.1",
    "name": "allCategory",
    "group": "3product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过账号密码登陆</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.id",
            "description": "<p>分类id</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.pid",
            "description": "<p>父id</p>"
          }
        ]
      }
    },
    "filename": "./3product.js",
    "groupTitle": "3.0 商品模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/product/allCategory.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/product/search.htm",
    "title": "3.03 根据商品名称进行搜索",
    "version": "0.0.1",
    "name": "search",
    "group": "3product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户令牌</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          },
          {
            "group": "com.haoxuer.umall.data.entity.Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>商品名称</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过账号密码登陆</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.id",
            "description": "<p>商品id</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "Success 200",
            "type": "list",
            "optional": false,
            "field": "list.catalog",
            "description": "<p>商品分类id</p> <p>apiSuccess {list} list.logo  商品图片</p>"
          }
        ]
      }
    },
    "filename": "./3product.js",
    "groupTitle": "3.0 商品模块",
    "sampleRequest": [
      {
        "url": "https://demo.haoxuer.com/rest/product/search.htm"
      }
    ]
  }
] });
