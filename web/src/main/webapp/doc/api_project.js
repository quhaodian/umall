define({
  "name": "移动端接口",
  "version": "0.0.1",
  "description": "移动端接口",
  "title": "移动端接口",
  "url1": "http://localhost:8080/web",
  "url2": "http://demo.haoxuer.com",
  "url": "https://demo.haoxuer.com",
  "sampleUrl": "https://demo.haoxuer.com",
  "template": {
    "withCompare": true,
    "withGenerator": true,
    "forceLanguage": "zh_cn"
  },
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-07-01T15:44:47.188Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
